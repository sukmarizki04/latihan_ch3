package com.example.chaptertiga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chaptertiga.databinding.ActivityEndBinding
import com.example.chaptertiga.databinding.ActivityStartBinding

class ActivityStart : AppCompatActivity() {
    private lateinit var binding: ActivityStartBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sendata = Intent(this,ActivityEnd::class.java)
        sendata.putExtra("NAMA_LENGKAP","SUKMA RIZKI")
        sendata.putExtra("NAMA_PANGGILAN","RIZKI")
        sendata.putExtra("USIA",20)
        sendata.putExtra("ALAMAT","Aceh Utara")
        binding.button1.setOnClickListener { startActivity(sendata) }

        val bundle1 = Intent(this,ActivityEnd::class.java)
        val bundle = Bundle()
        bundle.putString("NAMA_LENGKAP","Sukma Rizki")
        bundle.putString("NAMA_PANGGILAN","Rizki")
        bundle.putInt("USIA",20)
        bundle.putString("ALAMAT","Aceh Utara")
        bundle1.putExtras(bundle)

        binding.button2.setOnClickListener { startActivity(bundle1) }
        //serializable
        val serialializabledata = Intent(this,ActivityEnd::class.java)
        val person = Datas( "sukma rizki","rizki",20,"aceh utara")
        serialializabledata.putExtra("Object_serializable",person)

        binding.button3.setOnClickListener { startActivity(serialializabledata) }

        val parcel = Intent(this,ActivityEnd::class.java)
        val parcelable = DataParcel("sukma","rizki",20,"aceh")
        parcel.putExtra("Object_Parcel",parcelable)
        binding.button4.setOnClickListener { startActivity(parcel) }

    }
}