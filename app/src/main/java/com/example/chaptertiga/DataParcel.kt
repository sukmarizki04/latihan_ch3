package com.example.chaptertiga

import android.os.Parcel
import android.os.Parcelable

data class DataParcel(val name:String?,val callname:String?, val usia:Int?, val alamat:String?):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString()
    ) {
    }
    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(name)
        p0?.writeString(callname)
        if (usia != null) {
            p0?.writeInt(usia)
        }
        p0?.writeString(alamat)
    }
    override fun describeContents(): Int {
        return 0

    }


    companion object CREATOR : Parcelable.Creator<DataParcel> {
        override fun createFromParcel(parcel: Parcel): DataParcel {
            return DataParcel(parcel)
        }

        override fun newArray(size: Int): Array<DataParcel?> {
            return arrayOfNulls(size)
        }
    }
}
