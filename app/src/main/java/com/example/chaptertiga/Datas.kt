package com.example.chaptertiga

import java.io.Serializable

data class Datas(val namalengkap:String,val namapanggilan:String,val usia:Int,val alamat:String):Serializable
