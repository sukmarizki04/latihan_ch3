package com.example.chaptertiga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chaptertiga.databinding.ActivityEndBinding

class ActivityEnd : AppCompatActivity() {
    private lateinit var binding: ActivityEndBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEndBinding.inflate(layoutInflater)
        setContentView(binding.root)
        intent.getStringExtra("NAMA_LENGKAP")?.let {
            binding.text1.text = """
           ${intent.getStringExtra("NAMA_LENGKAP")}
           ${intent.getStringExtra("NAMA_PANGGILAN")}
           ${     intent.getIntExtra("USIA", 0)}
           ${      intent.getStringExtra("ALAMAT")}
        """.trimIndent()
        }


        intent.extras?.getString("ALAMAT")?.let {
            val Bundle = intent.extras
            val name = Bundle?.getString("NAMA_LENGKAP")
            val nama = Bundle?.getString("NAMA_PANGGILAN")
            val tipe = Bundle?.getInt("USIA")
            val adress = Bundle?.getString("ALAMAT")
            binding.text1.text = "$name,$nama,$tipe,$adress"
        }

        intent.getSerializableExtra("Object_serializable")?.let {
            val serializabledata = intent.getSerializableExtra("Object_serializable") as Datas
            binding.text1.text = """
            ${serializabledata.namalengkap}
            ${serializabledata.namapanggilan}
            ${serializabledata.usia}
            ${serializabledata.alamat}
        """.trimIndent()
        }
        intent.getParcelableExtra<DataParcel>("Object_Parcel")?.let {
            val parcelable = intent.getParcelableExtra<DataParcel>("Object_Parcel")
            binding.text1.text = """
            ${parcelable?.name}
            ${parcelable?.callname}
            ${parcelable?.usia}
            ${parcelable?.alamat}
        """.trimIndent()
        }
    }
}